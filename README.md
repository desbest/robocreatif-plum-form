# Robocreatif Plum Form

I didn't make this, robocreatif did. Their website is no longer online and this product is no longer sold.

plum.Form is the perfect tool for designers who need to step over the boundaries of boring web forms. It simplifies the process of creating perfect forms that look beautiful and add extra features to your web site.

[code canyon](https://codecanyon.net/user/robocreatif) [archive.is](https://archive.is/4XQmC)


## How does it work?

Other than a bit of JavaScript and some CSS , there’s really nothing extra you need. Plum focuses its power on forms or individual form elements and creates HTML wrappers on the fly. Instead of fiddling with a clumsy drop-down menu, you can use basic CSS classes to work with simple HTML . All you need is a form:

For every element that’s targeted, Plum creates a “wrapper” that you can style with CSS . That wrapper is given very basic, memorable class names, like “select”, “file”, or “input checkbox disabled”. Because Plum uses the type of element as a class name, you can add special styles to specific fields. For example, you might add a nice icon to fields that have a “search” class (using <input type="search">).

The underlying aspect of plum.Form is simplicity. It’s nothing more than a styling tool, which means you retain all functionality of a regular web form. Keyboard navigation still works (including typing the value of an option while focused on a select field), as does sending data back to your server. Plum is just there to make everything look nice and add an extra touch of flair.

# Browser Support

* Mozilla Firefox 3 or newer
* Apple Safari 3 or newer
* Google Chrome
* Opera 10 or newer
* Microsoft Internet Explorer 7 or newer

## Features

* No extra HTML markup is required
* Tiny file size! Minified, plum.Form is under 20 Kb
* Style all parts of a form, including file input fields, drop-down (select) menus, check boxes, radio buttons, buttons, and text and password fields
* Supports check box groups with multiple states: none, all and mixed (some checked, some not)
* In-field labels that fade on focus and hide while typing
* Form validation with pre-defined checks (phone #, email address and content length) as well as custom methods
* Supports multiple file uploads
* Files can be uploaded through AJAX
* HTML5 browsers can show a progress bar on AJAX file uploads
* Limit file uploads to certain types, sizes and quantities
* Retains standard form functionality: plum.Form supports focusing on elements with the tab key and navigating select menus by typing option names or using arrow keys
* Supports cascading select menus
* Easily customizable to create new form skins
* Extensive documentation
* Simple to use: include the files, target your forms, and you’re done!